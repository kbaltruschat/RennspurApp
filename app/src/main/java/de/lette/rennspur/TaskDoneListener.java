package de.lette.rennspur;

public interface TaskDoneListener {
    void onTaskSuccessful();

    void onTaskUnsuccessful();
}
