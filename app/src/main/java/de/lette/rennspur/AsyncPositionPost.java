package de.lette.rennspur;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

class AsyncPositionPost implements Runnable {
    private JSONObject jsonObject;
    private TaskDoneListener listener;
    private static URL url;

    public AsyncPositionPost(JSONObject jsonObject, TaskDoneListener listener) {
        this.jsonObject = jsonObject;
        this.listener = listener;
        System.out.println(jsonObject);
    }

    public static void setHostAddress(URL url) {
        AsyncPositionPost.url = url;
    }

    @Override
    public void run() {
        HttpURLConnection httpURLConnection = null;
        try {
            httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty("Content-Type", "application/json");
            httpURLConnection.connect();
            DataOutputStream wr = new DataOutputStream(httpURLConnection.getOutputStream());
            wr.writeBytes(jsonObject.toString());
            wr.flush();
            wr.close();


            BufferedReader r = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
            StringBuilder total = new StringBuilder();
            String line;
            while ((line = r.readLine()) != null) {
                total.append(line).append('\n');
            }
            r.close();
        } catch (IOException e) {
            listener.onTaskUnsuccessful();
        } finally {
            listener.onTaskSuccessful();
            httpURLConnection.disconnect();
        }
    }


}
