package de.lette.rennspur;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import java.net.MalformedURLException;
import java.net.URL;

public class RennspurActivity extends AppCompatActivity {

    private PositionManager positionManager;
    private LocationManager mgr;
    private LocationListener listener;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rennspur);

        Intent appLinkIntent = getIntent();
        Uri appLinkData = appLinkIntent.getData();
        try {
            String host = appLinkData.getQueryParameter("host");
            String hash = appLinkData.getQueryParameter("hash");

            positionManager = new PositionManager(hash);
            AsyncPositionPost.setHostAddress(new URL(host));


            mgr = (LocationManager) getSystemService(LOCATION_SERVICE);
            listener = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    positionManager.add(new Position(location));
                    new Thread(new AsyncPositionPost(positionManager.toJson(), new TaskDoneListener() {
                        @Override
                        public void onTaskSuccessful() {
                            positionManager.clear();
                        }

                        @Override
                        public void onTaskUnsuccessful() {
                            // do nothing
                        }
                    })).start();
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {
                }

                @Override
                public void onProviderEnabled(String provider) {
                }

                @Override
                public void onProviderDisabled(String provider) {
                }
            };


            if (checkPermissions()) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.INTERNET}, 0x68E);
            } else {
                mgr.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, listener);

            }
        } catch (NullPointerException | MalformedURLException | SecurityException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        for (int i : grantResults)
            if (i == PackageManager.PERMISSION_DENIED) {
                Toast.makeText(getApplicationContext(), "Accept permission request or the App will not work properly.", Toast.LENGTH_LONG).show();
                return;
            }
        if (checkPermissions())
            try {
                mgr.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, listener);
            } catch (SecurityException e) {
                //cannot happen, just to satisfy the IDE
                e.printStackTrace();
            }
    }


    private boolean checkPermissions() {
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED;
    }
}
